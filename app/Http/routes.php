<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->get('/about', function (){
    return "aaa";
});

$app->post('/login/', 'AuthController@authenticate');

$app->post('/register', 'UserController@register');

$app->get('/users', 'UserController@getUserInformation');

$app->post('/users/edit/gcm-token','UserController@updateGcmToken');

$app->post('/users/edit/password', 'UserController@updateProfilePassword');

$app->post('/users/edit/contact', 'UserController@updateProfileContact');

$app->post('/users/edit/address','userController@updateProfileAddress');

$app->post('/users/edit/profile','UserController@updateProfile');

$app->post('/users/edit/child', 'UserController@updateChildProfile');

$app->post('/users/edit/photo', 'UserController@changePhoto');

$app->get('/users/bid','UserController@getBiddingData');

$app->get('/users/bid/on-progress','UserController@getBiddingOnProgressData');

$app->get('/users/bidder','UserController@getBidderData');

$app->get('/users/bidder/on-progress','UserController@getBidderOnProgressData');

$app->get('/users/history', 'UserController@getDonorHistory');

$app->get('/bid/detail', 'ApplicationController@getBidDetail');

$app->post('/bid/accept','ApplicationController@acceptBid');

$app->post('/bid/finish','ApplicationController@finishBid');

$app->post('/donor', 'ApplicationController@addDonor');

$app->post('/donor/campaign/bid','ApplicationController@bidDonor');

$app->get('/donor/campaign/by-radius','ApplicationController@getNearestCampaignByRadius');

$app->get('/tgl', 'UserController@tgl');

