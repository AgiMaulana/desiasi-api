<?php
/**
 * Created by PhpStorm.
 * User: Agi Maulana
 * Date: 2/27/2016
 * Time: 1:27 PM
 */

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\UserController as User;

class AuthController extends Controller{

    public function authenticate(Request $request){
        if(empty($request->header('X-GCM-TOKEN-X')))
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "GCM Token Missed"
            ));

        $param = $request->all();
        $user = DB::table('user')->where('email', $param['email'])->count();
        if($user == 0)
            return array(
                "status" => false,
                "response_code" => 400,
                "message" => "Login failed. Email not registered."
            );

        $user = DB::table('user')->where('email', $param['email'])->first();
        $param['password'] = User::generatePassword($param['password'],$user->user_id);
        $user = DB::table('user')->where($param)->count();
        if($user == 0)
            return array(
                "status" => false,
                "response_code" => 400,
                "message" => "Login failed. Email and password doesn't match",
                "password" => $param['password']
            );


        $dateTime = new \DateTime();
        $date = $dateTime->format('H:i:s d-F-Y');
        $key = sha1($date);
        $user = DB::table('user')->where($param)->first();
        $token = sha1(sha1($key).$user->user_id);

        $updateValue = array(
            'remember_token' => $token,
            'gcm_token' => $request->header('X-GCM-TOKEN-X')
        );
        DB::table('user')->where($param)->update($updateValue);

        $user = DB::table('user')
                ->LeftJoin('religion', 'user.rel_code', '=', 'religion.rel_code')
                ->where('user.user_id', '=', $user->user_id)->first();

        return array(
            "status" => true,
            "response_code" => 200,
            "message" => "Login succesful",
            "token" => self::createToken($token, $user->user_id),
            "user" => $this->userToJSON($user)
        );

    }

    public function logout(Request $request){
        $param = $request->all();
        if(!isset($param['token']) || !self::authorizeToken($param['token']))
            return self::tokenFailedResponse();

        return array(
            "status" => true,
            "response_code" => 200,
            "message" => "Logged out"
        );

    }

    private function userToJSON($user){
//        if($user->gender != null){
//            if($user->gender == 'm')
//                $user->gender = 'Laki - laki';
//            else
//                $user->gender = 'Perempuan';
//        }

        $children = DB::table('children')->where('user_id', $user->user_id)->first();
    /*    if($children != null){
            if($children->gender == 'm')
                $children->gender = 'Laki - laki';
            else
                $children->gender = 'Perempuan';
        }*/

        if($children != null){
            $children = array(
                'gender' => $children->gender,
                'birthdate' => $children->birthdate,
                'weight' => $children->weight
            );
        }

        return array(
            'user_id' => $user->user_id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'gender' => $user->gender,
            'birthdate' => $user->birthdate,
            'job' => $user->job,
            'email' => $user->email,
            'phone_number' => $user->phone_number,
            'religion' => $user->rel_code,
            'profile_picture' => "http://192.168.43.185" . $user->profile_picture,
            'address' => array(
                'detail' => $user->address_detail,
                'city' => $user->address_city,
                'province' => $user->address_province
            ),
            'children' => $children
        );
    }

    public static function createToken($remember_token, $uid){
        $arr = array(
            "token" => $remember_token,
            "uid" => $uid
        );

        return base64_encode(json_encode($arr));
    }

    public static function authorizeToken($token){
        $decode = json_decode(base64_decode($token));
        if(!$decode)
            return false;

        $where = array(
            "user_id" => $decode->uid,
            "remember_token" => $decode->token
        );
        $user = DB::table('user')->where($where)->count();
        if($user == 1)
            return true;

        return false;
    }

    public static function parseUserIdFromToken($token){
        $decode = json_decode(base64_decode($token));
        if(!$decode)
            return false;
        return $decode->uid;
    }

    public static function tokenFailedResponse(){
        return json_encode(array(
            "status" => false,
            "response_code" => 400,
            "message" => "Token authentication failed. Please to re-login."
        ));
    }

    public function tgl(Request $request){
        $param = $request->all();
        if(isset($param['a']))
            return "a";

        $arr = array(
            "token" => "hd82732723hd92hd98091392039uj93-d0",
            "user_id" => "2038278397"
        );

        $encode = base64_encode(json_encode($arr));
        $decode = json_decode(base64_decode($encode));
        $a = self::authorizeToken("eyJ0b2tlbiI6IjA5MGRhNWQ5MjY3NTQwNjhhN2Q1YjNkMWFiNWQ4NjdiYWUwNDBlN2EiLCJ1aWQiOiIxMzAyOTgyODAxMzMyMjkifQ==") ? 1 : 0;
        return $a;
    }

} 