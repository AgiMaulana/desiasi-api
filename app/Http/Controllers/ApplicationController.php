<?php
/**
 * Created by PhpStorm.
 * User: Agi Maulana
 * Date: 4/8/2016
 * Time: 10:16 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\AuthController as AuthC;
use Illuminate\Support\Facades\DB;

class ApplicationController extends Controller {

    private function createDonorId(){
        $char = '0123456789';
        $id = "DNR";
        while(strlen($id) < 15){
            $id .= $char[rand(0, 9)];
        }

        $check = DB::table('donor')->where('donor_id', '=', $id)->count();
        if($check > 0)
            $id = $this->createDonorId();


        return $id;
    }

    private function createBidId(){
        $char = '0123456789';
        $id = "BID";
        while(strlen($id) < 15){
            $id .= $char[rand(0, 9)];
        }

        $check = DB::table('donor_bidder')->where('bid_id', '=', $id)->count();
        if($check > 0)
            $id = $this->createDonorId();


        return $id;
    }

    public function addDonor(Request $request){
        if(empty($request->header('X-DESI-TOKEN-X')) || !AuthC::authorizeToken($request->header('X-DESI-TOKEN-X')))
            return AuthC::tokenFailedResponse();

        $uid = AuthC::parseUserIdFromToken($request->header('X-DESI-TOKEN-X'));
        $param  = $request->all();

        $releaseDate = new \DateTime("now");
        $dateTime = new \DateTime("now");
        $expireDate = $dateTime->add(new \DateInterval("P". $param['donor_length'] ."D"));

        $countSelector = array(
            "user_id" => $uid
        );
        $donorCount = DB::table("donor")->where($countSelector)->count();
        if($donorCount > 0){
            $donor = DB::table("donor")->where($countSelector)->orderBy('created_at','DESC')->first();
            if($releaseDate->format("Y-m-d H:i:s:u") <= $donor->expire_date && $donor->is_active == 1)
                return json_encode(array(
                    "status" => false,
                    "response_code" => 400,
                    "message" => "Tidak dapat melakukan kampanye donor. Anda memiliki kampanye donor aktif ". $donor->donor_id ."."
                ));
        }

        $newCampaign = array(
            "donor_id" => $this->createDonorId(),
            "user_id" => $uid,
            "milk_quantity" => $param['quantity'],
            "address_province" => $param['address_province'],
            "address_city" => $param['address_city'],
            "address_detail" => $param['address_detail'],
            "address_lat" => $param['address_lat'],
            "address_lng" => $param['address_lng'],
            "expire_date" => $expireDate->format("Y-m-d H:i:s:U"),
            "is_active" => 1
        );

        $addDonor = DB::table('donor')->insert($newCampaign);
        if(!$addDonor){
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Tidak dapat menambahkan kampanye donor. Coba beberapa saat lagi."
            ));
        }

        return json_encode(array(
            "status" => true,
            "response_code" => 200,
            "message" => "Kampanye donor berhasil ditambahkan"
        ));

    }

    public function getLatLngDistance($lat1, $lon1, $lat2, $lon2){
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return ($miles * 1.609344);
    }

    public function getNearestCampaignByRadius(Request $request){
        $params = $request->all();
        $lat = $params['lat'];
        $lng = $params['lng'];
        $rad = $params['radius'];
        $today = new \DateTime("now");
        $donors = DB::table('donor')
            ->join('user', 'donor.user_id', '=', 'user.user_id')
            ->select('donor.*','user.user_id','user.first_name', 'user.last_name', 'user.rel_code')
            ->where('donor.expire_date', '>=', $today->format("Y-m-d H:i:s:u"))->get();
        if(!$donors){
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Kampanye donor tidak ditemukan",
                "campaigns" => array()
            ));
        }

        $selectedDonor = array();
        foreach ($donors as $donor) {
            $dnrLat = $donor->address_lat;
            $dnrLng = $donor->address_lng;
            if($this->getLatLngDistance($lat, $lng, $dnrLat, $dnrLng) <= $rad)
                $selectedDonor[] = $donor;
        }

        if(sizeof($selectedDonor) == 0)
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Kampanye donor tidak ditemukan",
                "campaigns" => array()
            ));

        return json_encode(array(
            "status" => true,
            "response_code" => 200,
            "message" => "Kampanye tersedia",
            "campaigns" => $selectedDonor
        ));
    }

    public function getCampaignByAddress(Request $request){
        $params = $request->all();
        $type = $params['type'];
        $address = $params['address'];

        if($type == 0)
            $donors = DB::table('donor')
            ->join('user', 'donor.user_id', '=', 'user.user_id')
            ->select('donor.*','user.user_id','user.first_name', 'user.last_name', 'user.rel_code')
            ->where('donor.address_city', '=', $address)
            ->where('donor.is_active','=',1)->get();
        else
            $donors = DB::table('donor')
                ->join('user', 'donor.user_id', '=', 'user.user_id')
                ->select('donor.*','user.user_id','user.first_name', 'user.last_name', 'user.rel_code')
                ->where('donor.address_province', '=', $address)->get();

        if(!$donors){
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Kampanye donor tidak ditemukan",
                "campaigns" => array()
            ));
        }

        return json_encode(array(
            "status" => true,
            "response_code" => 200,
            "message" => "Kampanye tersedia",
            "campaigns" => $donors
        ));
    }

    public function bidDonor(Request $request){
        if(empty($request->header('X-DESI-TOKEN-X')) || !AuthC::authorizeToken($request->header('X-DESI-TOKEN-X')))
            return AuthC::tokenFailedResponse();

        $uid = AuthC::parseUserIdFromToken($request->header('X-DESI-TOKEN-X'));
        $param  = $request->all();
        $campaignId = $param['donor_id'];

        $data = array(
            "donor_id" => $campaignId,
            "user_id" => $uid
        );

        $isHasBidding = DB::table('donor_bidder')->where($data)->count();
        if($isHasBidding)
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Anda sudah melakukan permintaan donor ini."
            ));

        $bidId = $this->createBidId();
        $data['bid_id'] = $bidId;
        $data['status'] = -1;
        DB::table('donor_bidder')->insert($data);

        $bidderData = DB::table('user')->where('user_id', '=', $uid)->first();
        $bidDate = DB::table('donor_bidder')->where('bid_id', '=', $bidId)->first();

        $notificationData = array(
            "notification_type" => "new_bid",
            "bid_id" => $bidId,
            "bid_date" => $bidDate->date,
            "user" => array(
                "user_id" => $bidderData->user_id,
                "first_name" => $bidderData->first_name,
                "last_name" => $bidderData->last_name,
            )
        );

        $campaignOwner = DB::table('user')
            ->join('donor', 'user.user_id', '=', 'donor.user_id')
            ->join('donor_bidder', 'donor.donor_id', '=', 'donor_bidder.donor_id')
            ->select('user.gcm_token')
            ->where('donor_bidder.bid_id','=',$bidId)->first();

        $fields = array(
            'to' => $campaignOwner->gcm_token,
            'data' => $notificationData
        );

        GCM::sendPushNotification($fields);

        return json_encode(array(
            "status" => true,
            "response_code" => 200,
            "message" => "Permintaan terkirim"
        ));
    }

    public function getBidDetail(Request $request){
        $params = $request->all();

        $donorSelection = array(
            'donor.milk_quantity',
            'donor.release_date',
            'donor.expire_date',
            'donor.address_detail',
            'donor.address_city',
            'donor.address_province',
            'donor.address_lat',
            'donor.address_lng',
            'donor.user_id',
            'donor_bidder.bid_id'
        );
        $donor = DB::table('donor')
            ->join('donor_bidder','donor.donor_id','=','donor_bidder.donor_id')
            ->select($donorSelection)
            ->where('donor_bidder.bid_id','=',$params['bid_id'])
            ->first();

        $donor->release_date = $this->convertDate($donor->release_date);
        $donor->expire_date = $this->convertDate($donor->expire_date);

        $owner = DB::table('user')
            ->select('user.user_id','user.first_name','user.last_name')
            ->where('user_id','=',$donor->user_id)
            ->first();

        $bidder = DB::table('user')
            ->join('donor_bidder','donor_bidder.user_id', '=', 'user.user_id')
            ->select('user.user_id','user.first_name','user.last_name')
            ->first();

        $result = array(
            'detail_donor' => $donor,
            'owner' => $owner,
            'bidder'=> $bidder
        );

        return json_encode($result);
    }

    public function acceptBid(Request $request){
        $params = $request->all();
        $update = array('status' => 0);

        $accept = DB::table('donor_bidder')
            ->where('donor_bidder.bid_id','=',$params['bid_id'])
            ->update($update);

        if(!$accept)
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Terjadi masalah. Coba lagi."
            ));

        $owner = DB::table('user')
            ->join('donor','donor.user_id','=','user.user_id')
            ->join('donor_bidder','donor_bidder.donor_id','=','donor.donor_id')
            ->where('donor_bidder.bid_id','=',$params['bid_id'])
            ->first();
        $ownerName = $owner->first_name . " " . $owner->last_name;

        $bidder = DB::table('user')
            ->join('donor_bidder','donor_bidder.user_id','=','user.user_id')
            ->where('donor_bidder.bid_id','=',$params['bid_id'])
            ->first();

        $gcmToken = $bidder->gcm_token;
        $message = array(
            "notification_type" => 2,
            "message" => $ownerName . " menerima permintaan anda"
        );
        $fields = array(
            'to' => $gcmToken,
            'data' => $message
        );
        GCM::sendPushNotification($fields);

        return json_encode(array(
            "status" => true,
            "response_code" => 200,
            "message" => "Permintaan diterima"
        ));
    }

    public function finishBid(Request $request){
        $params = $request->all();
        $bidId = $params['bid_id'];
        $method = $params['method'];

        $receiver = array(
            'bid_id' => $bidId,
            'delivery_method' => $method
        );

        $finish = DB::table('donor_receiver')->insert($receiver);
        if(!$finish)
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Terjadi masalah. Coba lagi."
            ));

        $update = array('status' => 1);
        $accept = DB::table('donor_bidder')
            ->where('donor_bidder.bid_id','=',$bidId)
            ->update($update);

        $owner = DB::table('user')
            ->join('donor','donor.user_id','=','user.user_id')
            ->join('donor_bidder','donor_bidder.donor_id','=','donor.donor_id')
            ->where('donor_bidder.bid_id','=',$bidId)
            ->first();

       // $gcmTokenOwner = $owner->gcm_token;
        return json_encode($owner);
        $message = array(
            "notification_type" => 3,
            "message" => "Transaksi ASI selesai."
        );
        $fieldsOwner = array(
            'to' => $gcmTokenOwner,
            'data' => $message
        );

        $bidder = DB::table('user')
            ->join('donor_bidder','donor_bidder.user_id','=','user.user_id')
            ->where('donor_bidder.bid_id','=',$params['bid_id'])
            ->first();

        $gcmTokenBidder = $bidder->gcm_token;
        $message = array(
            "notification_type" => 3,
            "message" => "Transaksi ASI selesai."
        );
        $fieldsBidder = array(
            'to' => $gcmTokenBidder,
            'data' => $message
        );

        GCM::sendPushNotification($fieldsOwner);
        GCM::sendPushNotification($fieldsBidder);

        return json_encode(array(
            "status" => true,
            "response_code" => 200,
            "message" => "Permintaan diterima"
        ));

    }

    private function convertDate($date){
        $dayNames = array('Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at','Sabtu', 'Minggu');
        $monthNames = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
            'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $dt = new \DateTime($date);
        $day = $dayNames[$dt->format('N')-1];
        $month = $monthNames[ceil($dt->format('m'))];
        $year = $dt->format('Y');
        $result = $day . ", " . $dt->format('N') . " " . $month . " " . $year . " ". $dt->format('H:i');

        return $result;
    }

}