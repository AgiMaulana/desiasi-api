<?php
/**
 * Created by PhpStorm.
 * User: Agi Maulana
 * Date: 2/27/2016
 * Time: 7:43 AM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\AuthController as AuthC;


class UserController extends Controller{

    private function getNewId(){
        $numerik = '0123456789';
        $newId = "";
        while(strlen($newId) < 15){
            $newId .= $numerik[rand(0, 9)];
        }

        $user = DB::table('user')->where('user_id', '=', $newId)->count();
        if($user > 0)
            $newId = $this->getNewId();

        return $newId;
    }

    private function getNewChildId(){
        $numerik = '0123456789';
        $newId = "ch";
        while(strlen($newId) < 15){
            $newId .= $numerik[rand(0, 9)];
        }

        $user = DB::table('children')->where('children_id', '=', $newId)->count();
        if($user > 0) $newId = $this->getNewChildId();

        return $newId;
    }

    private function isExist($user_id){
        $user = DB::table('user')->where('user_id', $user_id)->count();
        if($user > 0) return true;

        return false;
    }

    public function register(Request $request){
        $param = $request->all();
        $param['user_id'] = $this->getNewId();
        $param['password'] = self::generatePassword($param['password'],$param['user_id']);
        $param['profile_picture'] = '/photos/no-picture.png';

        $user = DB::table('user')->where('email', '=', $param['email'])->count();
        if($user > 0){
            return array(
                "status" => false,
                "response_code" => 400,
                "message" => "Email already registered"
            );
        }

        $insert = DB::table('user')->insert($param);
        if($insert)
            $response = array(
                "status" => true,
                "response_code" => 200,
                "message" => "Register succesful"
            );
        else
            $response = array(
                "status" => false,
                "response_code" => 400,
                "message" => "Register failed"
            );

        return $response;
    }

    public function updateProfilePassword(Request $request){
        if(empty($request->header('X-DESI-TOKEN-X')) || !AuthC::authorizeToken($request->header('X-DESI-TOKEN-X')))
            return AuthC::tokenFailedResponse();

        $param = $request->all();
        $uid = AuthC::parseUserIdFromToken($request->header('X-DESI-TOKEN-X'));

        $countWhere = array(
            'user_id' => $uid,
            'password' => self::generatePassword($param['old_password'], $uid)
        );
        $count = DB::table('user')->where($countWhere)->count();
        if($count == 0)
            return array(
                "status" => false,
                "response_code" => 400,
                "message" => "Wrong password"
            );

        $update = array(
            "password" => self::generatePassword($param['new_password'], $uid)
        );
        $where = array(
            "user_id" => $uid
        );

        DB::table('user')->where($where)->update($update);

        return json_encode(
            array(
                "status" => true,
                "response_code" => 200,
                "message" => "Password updated successfully"
            )
        );
    }

    public function updateProfileContact(Request $request){
        if(empty($request->header('X-DESI-TOKEN-X')) || !AuthC::authorizeToken($request->header('X-DESI-TOKEN-X')))
            return AuthC::tokenFailedResponse();

        $param = $request->all();
        $update = array(
            "email" => $param['email'],
            "phone_number" => $param['phone_number']
        );

        $uid = AuthC::parseUserIdFromToken($request->header('X-DESI-TOKEN-X'));
        $where = array(
            "user_id" => $uid
        );

        DB::table('user')->where($where)->update($update);

        return json_encode(
            array(
                "status" => true,
                "response_code" => 200,
                "message" => "Contact updated successfully"
            )
        );
    }

    public function updateProfileAddress(Request $request){
        if(empty($request->header('X-DESI-TOKEN-X')) || !AuthC::authorizeToken($request->header('X-DESI-TOKEN-X')))
            return AuthC::tokenFailedResponse();

        $param = $request->all();

        $update = array(
            "address_city" => $param['city'],
            "address_province" => $param['province'],
            "address_detail" => $param['detail']
        );

        $uid = AuthC::parseUserIdFromToken($request->header('X-DESI-TOKEN-X'));
        $where = array("user_id" => $uid);
        DB::table('user')->where($where)->update($update);

        return json_encode(
            array(
                "status" => true,
                "response_code" => 200,
                "message" => "Updated updated successfully"
            )
        );

    }

    public function updateProfile(Request $request){
        if(empty($request->header('X-DESI-TOKEN-X')) || !AuthC::authorizeToken($request->header('X-DESI-TOKEN-X')))
            return AuthC::tokenFailedResponse();

        $param = $request->all();

        $update = array(
            'gender' => $param['gender'],
            'birthdate' => $param['birthdate'],
            'rel_code' => $param['religion'],
            'job' => $param['job']
        );

        $uid = AuthC::parseUserIdFromToken($request->header('X-DESI-TOKEN-X'));
        $where = array("user_id" => $uid);
        DB::table('user')->where($where)->update($update);

        return json_encode(
            array(
                "status" => true,
                "response_code" => 200,
                "message" => "Profile updated successfully"
            )
        );
    }



    public function updateChildProfile(Request $request){
        if(empty($request->header('X-DESI-TOKEN-X')) || !AuthC::authorizeToken($request->header('X-DESI-TOKEN-X')))
            return AuthC::tokenFailedResponse();

        $param = $request->all();
        $update = array(
            'gender' => $param['gender'],
            'birthdate' => $param['birthdate'],
            'weight' => $param['weight']
        );

        $uid = AuthC::parseUserIdFromToken($request->header('X-DESI-TOKEN-X'));
        $where = array("user_id" => $uid);

        $countCild = DB::table('children')->where($where)->count();
        if($countCild == 0){
            $update['children_id'] = $this->getNewChildId();
            $update['user_id'] = $uid;
            DB::table('children')->insert($update);
            return json_encode(
                array(
                    "status" => true,
                    "response_code" => 200,
                    "message" => "Child profile updated successfully"
                )
            );
        }


        DB::table('children')->where($where)->update($update);
        return json_encode(
            array(
                "status" => true,
                "response_code" => 200,
                "message" => "Child profile updated successfully"
            )
        );
    }

    public function updateGcmToken(Request $request){
        if(empty($request->header('X-DESI-TOKEN-X')) || !AuthC::authorizeToken($request->header('X-DESI-TOKEN-X')))
            return AuthC::tokenFailedResponse();

        $uid = AuthC::parseUserIdFromToken($request->header('X-DESI-TOKEN-X'));
        $gcmToken = $request->header('GCM-TOKEN');

        $update = array(
            "user_id" => $uid,
            "gcm_token" => $gcmToken
        );

        DB::table('user')->update($update);
    }


    public static function generatePassword($plaintextPassword, $uid){
        return md5(sha1($plaintextPassword).sha1($uid));
    }

    public function getUserInformation(Request $request){
        $params = $request->all();

        $where = array("user_id" => $params['user_id']);
        $user = DB::table('user')->where($where)->first();
        $child = DB::table('children')->where($where)->first();

        $user->address = array(
            "detail" => $user->address_detail,
            "city" => $user->address_city,
            "province" => $user->address_province
        );

        unset($user->password);
        unset($user->address_detail);
        unset($user->address_city);
        unset($user->address_province);
        unset($user->created_at);
        unset($user->updated_at);
        unset($user->remember_token);
        unset($user->gcm_token);

        if(!empty($child)){
            unset($child->children_id);
            unset($child->created_at);
            unset($child->updated_at);
        }

        if(empty($user))
            return null;


        $user->children = $child;
        return json_encode($user);

    }

    public function getBiddingData(Request $request){
        $params = $request->all();

        $data = DB::table('donor_bidder')
            ->join('donor','donor_bidder.donor_id','=','donor.donor_id')
            ->join('user','user.user_id','=','donor.user_id')
            ->select('donor_bidder.date','donor_bidder.status','user.first_name','user.last_name','user.profile_picture','donor.milk_quantity')
            ->where('donor_bidder.user_id','=',$params['user_id'])
            ->where('donor_bidder.status','<>',0)
            ->get();

        if(!$data)
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Tidak ditemukan"
            ));

        $response = array(
            'status' => true,
            'response_code' => 200,
            'message' => 'ditemukan'
        );

        foreach ($data as $item) {
            $bid = array(
                'owner_name' => $item->first_name . " " . $item->last_name,
                'quantity' => $item->milk_quantity,
                'bid_date' => $this->convertDate($item->date),
                'status' => $item->status
            );
            $response['data'][] = $bid;
        }

        return $response;
    }

    public function getBiddingOnProgressData(Request $request){
        $params = $request->all();

        $data = DB::table('donor_bidder')
            ->join('donor','donor_bidder.donor_id','=','donor.donor_id')
            ->join('user','user.user_id','=','donor.user_id')
            ->select('donor_bidder.date','donor_bidder.status','user.first_name','user.last_name','user.profile_picture','donor.milk_quantity')
            ->where('donor_bidder.user_id','=',$params['user_id'])
            ->where('donor_bidder.status','=',0)
            ->get();

        if(!$data)
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Tidak ditemukan"
            ));

        $response = array(
            'status' => true,
            'response_code' => 200,
            'message' => 'ditemukan'
        );

        foreach ($data as $item) {
            $bid = array(
                'owner_name' => $item->first_name . " " . $item->last_name,
                'quantity' => $item->milk_quantity,
                'bid_date' => $this->convertDate($item->date),
                'status' => $item->status
            );
            $response['data'][] = $bid;
        }

        return $response;
    }

    public function getBidderData(Request $request){
        $params = $request->all();

        $dt = new \DateTime('now');

        $selection = array(
            'donor.donor_id',
            'donor_bidder.bid_id',
            'donor_bidder.date',
            'donor_bidder.status',
            'user.user_id',
            'user.first_name',
            'user.last_name',
            'user.rel_code',
            'user.profile_picture',
            'children.gender',
        );

        /**
         * <array name="bid_status">
         *    <item>Menunggu</item>
         *    <item>Diterima</item>
         *    <item>Ditolak</item>
         *    <item>Kampanye kadaluarsa</item>
         * </array>
         */

        $data = DB::table('donor')
            ->join('donor_bidder','donor_bidder.donor_id','=','donor.donor_id')
            ->join('user','user.user_id','=','donor_bidder.user_id')
            ->leftJoin('children','children.user_id','=','donor_bidder.user_id')
            ->where('donor.expire_date', '>=', $dt->format("Y-m-d H:i:s:u"))
            ->where('donor.user_id','=',$params['user_id'])
            ->where('donor_bidder.status','<>',0)
            ->where('donor_bidder.status','<>',1)
            ->select($selection)
            ->get();

        if(!$data)
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Tidak ditemukan"
            ));

        $response = array(
            'status' => true,
            'response_code' => 200,
            'message' => 'ditemukan',
            'donor_id' => $data[0]->donor_id
        );

        foreach ($data as $item) {
            $bid = array(
                'bid_id' => $item->bid_id,
                'bid_date' => $this->convertDate($item->date),
                'bid_status' => $item->status,
                'bidder_user_id' => $item->user_id,
                'owner_name' => $item->first_name . " " . $item->last_name,
                'profile_picture' => $item->profile_picture,
                'rel_code' => $item->rel_code,
                'child_gender' => $item->gender
            );
            $response['data'][] = $bid;
        }

        return json_encode($response);
    }

    public function getBidderOnProgressData(Request $request){
        $params = $request->all();

        $dt = new \DateTime('now');

        $selection = array(
            'donor.donor_id',
            'donor_bidder.bid_id',
            'donor_bidder.date',
            'donor_bidder.status',
            'user.user_id',
            'user.first_name',
            'user.last_name',
            'user.rel_code',
            'user.profile_picture',
            'children.gender',
        );

        /**
         * <array name="bid_status">
         *    <item>Menunggu</item>
         *    <item>Diterima</item>
         *    <item>Ditolak</item>
         *    <item>Kampanye kadaluarsa</item>
         * </array>
         */

        $data = DB::table('donor')
            ->join('donor_bidder','donor_bidder.donor_id','=','donor.donor_id')
            ->join('user','user.user_id','=','donor_bidder.user_id')
            ->leftJoin('children','children.user_id','=','donor_bidder.user_id')
            ->where('donor.expire_date', '>=', $dt->format("Y-m-d H:i:s:u"))
            ->where('donor.user_id','=',$params['user_id'])
            ->where('donor_bidder.status','=',0)
            ->select($selection)
            ->get();

        if(!$data)
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Tidak ditemukan"
            ));

        $response = array(
            'status' => true,
            'response_code' => 200,
            'message' => 'ditemukan',
            'donor_id' => $data[0]->donor_id
        );

        foreach ($data as $item) {
            $bid = array(
                'bid_id' => $item->bid_id,
                'bid_date' => $this->convertDate($item->date),
                'bid_status' => $item->status,
                'bidder_user_id' => $item->user_id,
                'owner_name' => $item->first_name . " " . $item->last_name,
                'profile_picture' => $item->profile_picture,
                'rel_code' => $item->rel_code,
                'child_gender' => $item->gender
            );
            $response['data'][] = $bid;
        }

        return json_encode($response);
    }

    public function changePhoto(Request $request){
        $params = $request->all();
        $uid = $params['user_id'];
        $file = $request->file("file");
        $type = $file->getClientOriginalExtension();
        $filename = $uid .".". $type;
        $directory = "photos/";
        $size = $file->getSize() / 1000;

        $allowedType = array('jpg','png');

        if(!$file->isValid() || $size > 1000 || !in_array($type, $allowedType))
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Tipe file tidak diperbolehkan atau terlalu besar"
            ));


        $where = array('user_id' => $uid);
        $photo = array('profile_picture' => '/photos/'.$filename);
        $update = DB::table('user')->where($where)->update($photo);
        /*if(!$update) {
            return json_encode(array(
                "status" => false,
                "response_code" => 400,
                "message" => "Tipe file tidak diperbolehkan atau terlalu besar"
            ));
        }*/

        unlink($directory . $filename);
        $file->move($directory, $filename);

        return json_encode(array(
            "status" => true,
            "response_code" => 200,
            "message" => "Berhasil"
        ));
    }

    private function convertDate($date){
        $dayNames = array('Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at','Sabtu', 'Minggu');
        $monthNames = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni',
            'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $dt = new \DateTime($date);
        $day = $dayNames[$dt->format('N')-1];
        $month = $monthNames[ceil($dt->format('m'))];
        $year = $dt->format('Y');
        $result = $day . ", " . $dt->format('N') . " " . $month . " " . $year . " ". $dt->format('H:i');

        return $result;
    }

    public function getDonorHistory(Request $request){
        $params = $request->all();

        $campaignHistory = DB::select("select donor.*
            from donor where user_id ='". $params['user_id'] ."'");

        $succesfulHistory = DB::select("select donor.*
            from donor,donor_bidder,donor_receiver where donor_bidder.donor_id = donor.donor_id
            and donor_bidder.bid_id = donor_receiver.bid_id and donor.user_id = '". $params['user_id'] ."'");

        $receiveHistory = DB::select("SELECT
                   user.user_id,
                   user.first_name,
                   user.last_name,
                   donor.milk_quantity,
                   donor_receiver.date
                FROM
                   user,
                   donor,
                   donor_receiver,
                   donor_bidder
                WHERE
                   user.user_id = donor.user_id
                   and donor_bidder.bid_id = donor_receiver.bid_id
                   and donor_bidder.user_id = '". $params['user_id'] ."'");

        for($i = 0; $i < sizeof($receiveHistory); $i++)
            $receiveHistory[$i]->date = $this->convertDate($receiveHistory[$i]->date);

        $result = array(
            'campaigns' => $campaignHistory,
            'succesful' => $succesfulHistory,
            'receive' => $receiveHistory
        );

        return json_encode($result);
    }


    public function tgl(Request $request){

        $notificationData = array(
            "notification_type" => 1,
            "bid_id" => '123',
            "bid_date" => '21 Desember 1995',
            "user_id" => '34593',
            "first_name" => 'agi',
            "last_name" => 'maulana',
        );

        $fields = array(
            'to' => 'eVyDATDMv28:APA91bGF6KGl7m6aDQxbLqQV4Otw8G2PbVHJ-Eb8qcWKPtkyJJi4DtZ3Vd_-oLYloCW879izydmLMNk3RUzpIngIP9-tE_AwchiP8MKylEf8xdMvwXfU3ZeA0_KTqKL-egopaX86M0ed',
            'data' => $notificationData
        );
        //return GCM::sendPushNotification($fields);

        $dt = new \DateTime('now');$dt->format("Y-m-d H:i:s:u");
        $cek = DB::table('donor_bidder')
            ->join('donor_receiver','donor_receiver.bid_id','=', 'donor_bidder.bid_id')
            ->join('donor','donor.donor_id','=','donor_bidder.donor_id')
            ->where('donor.expire_date', '>=', $dt->format("Y-m-d H:i:s:u"))
            ->where('donor.user_id','=','263706800647996')
            ->get();

        return json_encode($cek);
    }

}